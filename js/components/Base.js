import React, { Component } from 'react';
import {
    View,
    Text
} from 'react-native';

import Config from '../environment';
import axios from 'axios';

import ListSchools from './ListSchools';

export default class Base extends Component {
    constructor() {
        super();

        axios.defaults.baseURL = Config.baseUrl;

        this.state = {};
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <ListSchools />
            </View>
        );
    }
}
