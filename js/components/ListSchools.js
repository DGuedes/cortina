import React, { Component } from 'react';
import {
    View,
    Text,
    Alert,
    ListView,
    TouchableNativeFeedback
} from 'react-native';

import axios from 'axios';

export default class ListSchools extends Component {
    constructor() {
        super();

        this.state = {
            schoolsDataSource: this.getSchoolsDataSource([])
        };
    }

    componentDidMount() {
        this.fetchSchoolsData();
    }

    getSchoolsDataSource(schools) {
        let dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1.code !== r2.code});
        return dataSource.cloneWithRows(schools);
    }

    fetchSchoolsData() {
        axios.get(`/rest/escolas`, {
                params: {
                    quantidadeDeItens: 5,
                    pagina: 0,
                    campos: "nome,codEscola"
                }
            })
            .then((response) => {
                console.log(response.data);

                let data = response.data.map((school) => {
                    return {
                        code: school.codEscola,
                        name: school.nome
                    }
                });

                this.setState({
                    schoolsDataSource: this.getSchoolsDataSource(data)
                });
            });
    }

    handleSchoolOnPress(evt, school) {
        console.log(school); // use react-native log-android to see this log
    }

    renderSchool(school) {
        return (
            <TouchableNativeFeedback onPress={(evt) => this.handleSchoolOnPress(evt, school)}>
                <View style={{marginBottom: 10, borderBottomWidth: 2, borderBottomColor: '#000000'}}>
                    <View>
                        <Text>Codigo: {school.code}</Text>
                    </View>

                    <View>
                        <Text>Nome: {school.name}</Text>
                    </View>
                </View>
            </TouchableNativeFeedback>
        );
    }

    render() {
        return (
            <View>
                <View style={{backgroundColor:'#d4d4d4', padding: 5}}>
                    <Text style={{fontSize: 20}}>List schools</Text>
                </View>

                <View>
                    <ListView
                        dataSource={this.state.schoolsDataSource}
                        renderRow={(rowData) => this.renderSchool(rowData)}
                    />
                </View>
            </View>
        );
    }
}

