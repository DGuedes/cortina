/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

import Base from './js/components/Base';

export default class cortina extends Component {
  render() {
    return (
        <Base />
    );
  }
}

AppRegistry.registerComponent('cortina', () => cortina);
